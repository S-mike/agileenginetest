#!/usr/bin/env node

const bunyan = require('bunyan');
const fs = require('fs');
const { JSDOM } = require('jsdom');
const logger = bunyan.createLogger({ name: "myapp" });
const _ = require('lodash')

const [,, ...args] = process.argv;
const htmlFilePathes = [];
const options = {
  useExactPathes: true,
  useCustomSelector: ''
};

const deafultSelector = '#make-everything-ok-button';
const counter = {};

process.on('beforeExit', () => {
  Object.entries(counter).map(item => {
    if(item[1] == 0){
      logger.error('File not found: ', item[0])
    }
  })
})

if(_.isEmpty(args)){
  logger.error('Nothing to do');
  process.exit(0);
}

for (let i = 0; i < args.length; i++){
  if(args[i].length === 2 && args[i][0] === '-'){
    if(args[i][1] === 'p'){
      options.useExactPathes = false;
    }
    else if(args[i][1] === 'i') {
      try {
        options.useCustomSelector = args[i+1];
        i += 1;
        continue;
      }
      catch(e){
        logger.warn('Can not identify custom use. Will be use default "#make-everything-ok-button" insted')
      }
      options.useCustomSelector = true;
    }
    else{
      logger.warn(`Uknown option "${args[i]}"`)
    }
  }
  else{
    htmlFilePathes.push(args[i])
  }
}

if(_.isEmpty(htmlFilePathes)){
  logger.error('Nothing to parse');
  process.exit(0);
}

htmlFilePathes.map((fileName, i) => {
  if(options.useExactPathes){
    htmlFilePathes[i] = fileName;
    counter[htmlFilePathes[i]] = 0;
  }
  else{
    try{
      htmlFilePathes[i] = fileName.split('/').slice(-1)[0];
      counter[htmlFilePathes[i]] = 0;
    }
    catch(e){
      logger.error('Cant handle filename', e)
    }
  }
})

if(options.useExactPathes){
  return findWithExatcPath();
}
else{
  return findFilePaths(process.cwd());
  
}

function findWithExatcPath() {
  htmlFilePathes.map(filePath => {
    return findElementInFile(filePath, (err, data) => {
      if(err) return logger.error(err);

      console.log(`${filePath}: ${data}`);
      counter[filePath] += 1;
    })    
  })
}

function findFilePaths(path){
  fs.readdir(path, (err, list) => {
    if(err) return logger.error(err);

    list.map(item => {
      if(htmlFilePathes.includes(item)){
        return findElementInFile(`${path}/${item}`, (err, data) => {
          if(err) return logger.error(err);
    
          console.log(`${path}/${item}: ${data}`);
          counter[item] += 1;
        })    
      }
      else{
        let nextPath = `${path}/${item}`;
        let stats = fs.statSync(nextPath);
        
        if(stats.isDirectory() && !(nextPath.includes('node_modules') || nextPath.includes('.git'))){
          return findFilePaths(`${path}/${item}`);
        }
      }
    })
  })
}

function getParent(element, previousTag = null) {
  previousTag = previousTag || [];
  if(element.parentNode){
    previousTag.push(element.tagName);
    return getParent(element.parentNode, previousTag);
  }

  return previousTag;
}

function findElementInFile(fileName, cb) {
  fs.readFile(fileName, (err, data) => {
    if (err) return logger.error(`Error trying to access file ${fileName}`, err);

      const dom = new JSDOM(data);
      let button = dom.window.document.querySelector(deafultSelector);
      if(button){
        return cb(null, getParent(button).join(' > '));
      }

      button = dom.window.document.querySelector('[onclick^="javascript:window.ok"]');
      if(button){
        return cb(null, getParent(button).join(' > '));
      }

      if(options.useCustomSelector !== ''){
        button = dom.window.document.querySelector(options.useCustomSelector);
        if(button){
          return cb(null, getParent(button).join(' > '));
        }
      }

      return cb(fileName + ': Error trying to find element by css selector');
  });
}