# AgileEngene test

This script is parsing html files to find specific button

`node index.js [-p] [-i cssSelector] filename1 filename2 ...`

## Options descriptions
`-p` 
    script try to find files in script root directory, ignores pathes which can be presence in file names

`-i someCssSeletor`
    script add to standart css selectors list seletctor "someCssSeletor". Warning: symbol "#" must escaped!, so use "\#" when run script via cli

## Additional features

You can use "find-button" command in CLI in case of add current script to global scope just run `npm link`

In the root folder there binary files(`find-button-script-linux`,`find-button-script-macos`,`find-button-script-win.exe`) for Linux, Mac OS and Windows, which use same parameters

## Examples

* `node index.js -p -i \#asd ./html/sample-0-origin.html ./html/sample-0-origin.html` 
* `find-button -p -i \#asd ./html/sample-0-origin.html ./html/sample-0-origin.html `
* `find-button-script-macos -p -i \#asd ./html/sample-0-origin.html ./html/sample-0-origin.html`
